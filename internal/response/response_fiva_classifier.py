import os
import traceback
import typing
import uuid
import json
import pickle as pkl
import yaml
import os
import ast
import re

from typing import Any, Optional, Text, Dict, List, Type
from sentence_transformers import SentenceTransformer
from sklearn.neural_network import MLPClassifier
from joblib import dump, load

from rasa.nlu.components import Component
from rasa.nlu.config import RasaNLUModelConfig
from fuzzywuzzy import fuzz, process

from internal.bucket.mongodb import question_category_col, question_col, entity_col


if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata

class ResponseFivaClassifier(Component):
    """A new component"""

    # Which components are required by this component.
    # Listed components should appear before the component itself in the pipeline.
    @classmethod
    def required_components(cls) -> List[Type[Component]]:
        """Specify which components need to be present in the pipeline."""

        return []

    # Defines the default configuration parameters of a component
    # these values can be overwritten in the pipeline configuration
    # of the model. The component should choose sensible defaults
    # and should be able to create reasonable results with the defaults.
    defaults = {}

    # Defines what language(s) this component can handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    supported_language_list = None

    # Defines what language(s) this component can NOT handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    not_supported_language_list = None

    def __init__(self, component_config: Optional[Dict[Text, Any]] = None) -> None:
        super().__init__(component_config)

        self.__svm_model = None
        self.__d_variation_label = {}
        self.__l_variation = []
        self.__embedding_transformer = SentenceTransformer('xlm-r-100langs-bert-base-nli-stsb-mean-tokens')
        self.__init_component()

    def __get_overlap_token_score(self, query, l_choice):
        return process.extract(query, l_choice, limit=20, scorer=fuzz.token_sort_ratio)

    def __init_component(self):
        if os.path.exists('./custom_model/svm_model.pkl'):
            self.__svm_model = load('./custom_model/svm_model.pkl')

        if os.path.exists('./custom_model/variation_label.pkl'):
            with open('./custom_model/variation_label.pkl', 'rb') as f:
                self.__d_variation_label = pkl.load(f)

        if os.path.exists('./custom_model/label_variation.pkl'):
            with open('./custom_model/label_variation.pkl', 'rb') as f:
                self.__d_label_variation = pkl.load(f)

        if os.path.exists('./custom_model/label_answer.pkl'):
            with open('./custom_model/label_answer.pkl', 'rb') as f:
                self.__d_label_answer = pkl.load(f)

    def train(
        self,
        training_data,
        config: Optional[RasaNLUModelConfig] = None,
        **kwargs: Any,
    ) -> None:
        """Train this component.

        This is the components chance to train itself provided
        with the training data. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.train`
        of components previous to this one."""

        d_question = {}
        d_utter_answer = {}

        l_question_category_record = question_category_col.find({})
        for question_category_record in l_question_category_record:
            l_question_record = question_col.find({'question_category_id': question_category_record['_id']})

            intent_name = question_category_record['intent_name']
            d_utter_answer[intent_name] = question_category_record['response']

            d_question[intent_name] = []
            for question_record in l_question_record:
                d_question[intent_name].append(question_record['question'].lower())

        d_variation_label = {}
        d_label_variation = {}
        d_label_answer = {}

        l_X = []
        l_y = []
        for intent_name in d_question:
            for variation in d_question[intent_name]:
                d_variation_label[variation] = intent_name
                d_label_variation[intent_name] = variation
                d_label_answer[intent_name] = d_utter_answer[intent_name]
                l_X.append(variation)
                l_y.append(intent_name) 

        X = self.__embedding_transformer.encode(l_X)
        svm_model = MLPClassifier(hidden_layer_sizes=(300, 200, 500, 100), max_iter=500, activation = 'tanh',solver='adam',random_state=1)
        svm_model.fit(X, l_y)

        self.__d_variation_label = d_variation_label
        self.__d_label_variation = d_label_variation
        self.__d_label_answer = d_label_answer

        self.__svm_model = svm_model

    def __get_answer_score(self, query):
        self.__init_component()

        l_related_result = []
        s_related_label = set()
        l_variation = []

        for variation in self.__d_variation_label:
            l_variation.append(variation)

        l_overlap_token_score = self.__get_overlap_token_score(query, l_variation)

        most_overlap_token_score = l_overlap_token_score[0][1] / 100
        most_overlap_token_question = l_overlap_token_score[0][0]

        fuzzy_label = str(self.__d_variation_label[most_overlap_token_question])
        fuzzy_score = most_overlap_token_score

        X_test = self.__embedding_transformer.encode([query])
        transformer_score, transformer_label = sorted(zip(self.__svm_model.predict_proba(X_test)[0], self.__svm_model.classes_), reverse=True)[0]
        transformer_label = str(transformer_label)

        if most_overlap_token_score >= 0.9:
            print("Fuzzy prediction")
            label = fuzzy_label
            score = fuzzy_score

        else:
            print("Transformer prediction")
            label = transformer_label
            score = transformer_score

        s_related_label.add(label)
        if transformer_score > 0.6:
            if transformer_label not in s_related_label:
                l_related_result.append({
                    'intent_name': transformer_label,
                    'question': self.__d_label_variation[transformer_label],
                    'response': self.__d_label_answer[transformer_label],
                    'score': transformer_score
                })
                s_related_label.add(transformer_label)

        for overlap_result in l_overlap_token_score:
            cur_label = self.__d_variation_label[overlap_result[0]]
            if cur_label in s_related_label:
                continue

            if cur_label.strip().lower() == 'faqall':
                continue

            if len(s_related_label) == 4:
                break

            l_related_result.append({
                'intent_name': cur_label,
                'question': overlap_result[0],
                'response': self.__d_label_answer[cur_label],
                'score': overlap_result[1] / 100
            })
            s_related_label.add(cur_label)

        label = str(label)
        sample_question = self.__d_label_variation[label]

        if score >= 0:
            print('-------------')
            print('Prediction score: ', score)
            print('-------------')
            print('Query: ', query)
            print('-------------')
            print('Sample question: ', sample_question)
            print('-------------')
            print('Answer: ', self.__d_label_answer[label])
            print('-------------')
            print('Related result: ', l_related_result)
            print("=============")
        else:
            print("I dont understand")

        return label, self.__d_label_answer[label], score, l_related_result 

    def __get_entity_lookup(self, raw_text):
        def is_contain_entity(txt, entity):
            res = re.search('(^|[^0-9a-zA-Z]){}([^0-9a-zA-Z]|$)'.format(entity), txt, re.IGNORECASE)
            return res is not None

        l_entity = []
        l_entity_record = entity_col.find({})

        for entity_record in l_entity_record:
            entity = entity_record['entity'].strip()
            if is_contain_entity(raw_text, entity):
                l_entity.append(entity)
        
        return l_entity

    def process(self, message, **kwargs: Any) -> None:
        """Process an incoming message.

        This is the components chance to process an incoming
        message. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.process`
        of components previous to this one."""
        try:
            d_message = message.as_dict()
            raw_text = d_message['text']
            l_entity = self.__get_entity_lookup(raw_text)
            print(l_entity)
            label, raw_answer, score, l_related_result = self.__get_answer_score(raw_text)
            l_related_result = sorted(l_related_result, key=lambda k: k['score'])[::-1]

            message.set("intents", [{"intent_name": label, "confidence": float(score), 'response': raw_answer}], add_to_output=True)
            message.set("related_result", l_related_result, add_to_output=True)
            message.set("entities", l_entity, add_to_output=True)

        except Exception as e:
            print(e)
            traceback.print_exc()

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this component to disk for future loading."""
        dump(self.__svm_model, './custom_model/svm_model.pkl')

        with open('./custom_model/variation_label.pkl', 'wb') as f:
            pkl.dump(self.__d_variation_label, f)
        
        with open('./custom_model/label_variation.pkl', 'wb') as f:
            pkl.dump(self.__d_label_variation, f)
        
        with open('./custom_model/label_answer.pkl', 'wb') as f:
            pkl.dump(self.__d_label_answer, f)

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any,
    ) -> "Component":
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            return cls(meta)
