import os
import pymongo

mongodb_uri = os.environ.get('DB_URI') 
mongodb_client = pymongo.MongoClient(mongodb_uri, connect=True)
mongodb_database = mongodb_client[os.environ.get('DB_NAME')]

question_category_col = mongodb_database['QuestionCategory']
question_col = mongodb_database['Question']
entity_col = mongodb_database['Entity']
